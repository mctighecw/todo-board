import React, { useState } from 'react';
import { checkUser, saveUser } from 'Utils/functions';
import './styles.less';

import BoardMenu from 'Components/BoardMenu/BoardMenu';
import ListContainer from 'Components/ListContainer/ListContainer';

const Board = () => {
  const user = checkUser();
  const [selectedUser, setSelectedUser] = useState(user);
  const [selectedFilter, setSelectedFilter] = useState('all');

  const handleSetUser = (username) => {
    setSelectedUser(username);
    saveUser(username);
  };

  return (
    <div styleName="container">
      <BoardMenu
        selectedUser={selectedUser}
        selectedFilter={selectedFilter}
        handleSetUser={handleSetUser}
        handleSetFilter={setSelectedFilter}
      />
      <ListContainer selectedFilter={selectedFilter} />
    </div>
  );
};

export default Board;
