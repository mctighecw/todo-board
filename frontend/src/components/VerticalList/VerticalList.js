import React from 'react';
import { Droppable } from 'react-beautiful-dnd';

import { useApolloClient, useMutation } from '@apollo/react-hooks';
import { checkUser, removeItem } from 'Utils/functions';
import { UPDATE_LISTS } from 'Graphql/mutations';
import { GET_CONFIG } from 'Graphql/queries';

import ToDoItem from 'Components/ToDoItem/ToDoItem';
import CloseIcon from 'Assets/close-icon-white.svg';
import './styles.less';

const VerticalList = ({ title, listId, selectedFilter, items, lists }) => {
  const client = useApolloClient();
  const selectedUser = checkUser();

  const getListStyle = (isDraggingOver) => ({
    opacity: isDraggingOver ? 0.9 : 1,
  });

  const [updateLists] = useMutation(UPDATE_LISTS, {
    refetchQueries: [
      {
        query: GET_CONFIG,
        variables: { user: selectedUser },
      },
    ],
  });

  return (
    <Droppable droppableId={listId}>
      {(provided, snapshot) => (
        <div
          {...provided.droppableProps}
          ref={provided.innerRef}
          styleName="list-box"
          style={getListStyle(snapshot.isDraggingOver)}
        >
          <div>
            <div styleName="title">{title}</div>
            {selectedFilter === 'all' && listId !== 'backlog' && items.length === 0 && (
              <div
                styleName="delete-icon"
                onClick={(event) =>
                  updateLists({ variables: { user: selectedUser, lists: removeItem(listId, lists) } })
                }
              >
                <img src={CloseIcon} alt="Delete" />
              </div>
            )}
          </div>
          {items.length > 0 &&
            items.map((item, index) => <ToDoItem key={item.id} item={item} index={index} />)}
          {provided.placeholder}
        </div>
      )}
    </Droppable>
  );
};

export default VerticalList;
