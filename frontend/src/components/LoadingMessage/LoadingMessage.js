import React from 'react';
import LoadingSpinner from 'Assets/loading-spinner.svg';
import './styles.less';

const LoadingMessage = () => (
  <div styleName="container">
    <img src={LoadingSpinner} alt="." />
    <div styleName="message">Loading data...</div>
  </div>
);

export default LoadingMessage;
