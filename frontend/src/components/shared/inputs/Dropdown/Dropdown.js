import React from 'react';
import './styles.less';

const Dropdown = ({ placeholder, value, name, options, onChangeMethod }) => (
  <div styleName="dropdown">
    <select value={value} name={name} onChange={onChangeMethod}>
      <option value="">{placeholder}</option>
      {options.length > 0 &&
        options.map((item, index) => (
          <option value={item} key={index}>
            {item}
          </option>
        ))}
    </select>
  </div>
);

export default Dropdown;
