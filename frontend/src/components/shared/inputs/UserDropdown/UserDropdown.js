import React from 'react';
import './styles.less';

const UserDropdown = ({ value, options, onChangeMethod }) => (
  <div styleName="dropdown">
    <select value={value} onChange={onChangeMethod}>
      {options.length > 0 &&
        options.map((item, index) => (
          <option value={item.username} key={index}>
            {item.username}
          </option>
        ))}
    </select>
  </div>
);

export default UserDropdown;
