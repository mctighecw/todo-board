import React from 'react';
import './styles.less';

const OutlineButton = ({ label, disabled, onClickMethod }) => (
  <button styleName="outline-button" disabled={disabled || false} onClick={onClickMethod}>
    {label}
  </button>
);

export default OutlineButton;
