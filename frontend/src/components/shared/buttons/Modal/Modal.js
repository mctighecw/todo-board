import React from 'react';
import './styles.less';

const ModalButton = ({ label, onClickMethod }) => (
  <button styleName="modal-button" onClick={onClickMethod}>
    {label}
  </button>
);

export default ModalButton;
