import React from 'react';
import './styles.less';

const SmallButton = ({ label, disabled, onClickMethod }) => (
  <button styleName="small-button" disabled={disabled || false} onClick={onClickMethod}>
    {label}
  </button>
);

export default SmallButton;
