import React from 'react';
import './styles.less';

const BigButton = ({ label, disabled, onClickMethod }) => (
  <button styleName="big-button" disabled={disabled || false} onClick={onClickMethod}>
    {label}
  </button>
);

export default BigButton;
