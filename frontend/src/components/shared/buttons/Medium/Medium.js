import React from 'react';
import './styles.less';

const MediumButton = ({ label, type, disabled, onClickMethod }) => {
  const style = `medium-button ${type}`;

  return (
    <button styleName={style} disabled={disabled || false} onClick={onClickMethod}>
      {label}
    </button>
  );
};

export default MediumButton;
