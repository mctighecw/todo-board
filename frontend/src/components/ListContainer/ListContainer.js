import React from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import { checkUser, makeKey, addItem, getListItemVars } from 'Utils/functions';
import { filterData, loadData } from 'Utils/listHelpers';

import { useQuery, useMutation } from '@apollo/react-hooks';
import { GET_CONFIG, GET_USER_TODOS, GET_CONFIG_AND_TODOS } from 'Graphql/queries';
import { UPDATE_TODO_LOCAL, UPDATE_TODO_REMOTE, UPDATE_LISTS } from 'Graphql/mutations';

import LoadingMessage from 'Components/LoadingMessage/LoadingMessage';
import ErrorMessage from 'Components/ErrorMessage/ErrorMessage';
import EditFields from 'Components/EditFields/EditFields';
import VerticalList from 'Components/VerticalList/VerticalList';

import './styles.less';

if (process.env.NODE_ENV === 'development') {
  // disable warnings
  window['__react-beautiful-dnd-disable-dev-warnings'] = true;
}

const ListContainer = ({ selectedFilter }) => {
  const selectedUser = checkUser();
  const { loading, error, data } = useQuery(GET_CONFIG_AND_TODOS, { variables: { user: selectedUser } });

  const renderLists = (data) => {
    const { config, userTodos } = data;
    const result = [];

    config[0].lists.map((list, index) => {
      const key = makeKey(list);
      let items = [];

      if (userTodos && userTodos.length > 0) {
        const sortedData = loadData(userTodos, config[0].lists);
        const filteredData = filterData(sortedData, selectedFilter, data.config[0].lists);
        items = filteredData[key] ? filteredData[key] : [];
      }

      return result.push(
        <div key={index} styleName="list">
          <VerticalList
            title={list}
            listId={key}
            items={items}
            selectedFilter={selectedFilter}
            lists={config[0].lists}
          />
        </div>
      );
    });

    return result;
  };

  const onDragEnd = (result) => {
    const { source, destination } = result;

    if (!destination) {
      // no destination list
      return;
    } else {
      const { userTodos, config } = data;
      const stateData = loadData(userTodos, config[0].lists);
      const selectedItem = stateData[source.droppableId].find((item) => item.id === result.draggableId);
      const itemIndex = stateData[source.droppableId].indexOf(selectedItem);

      if (source.droppableId === destination.droppableId) {
        // move item within a list

        for (let index in stateData[source.droppableId]) {
          const item = stateData[source.droppableId][index];
          const descending = selectedItem.listPosition < destination.index;

          // assign a new position to each list item
          if (selectedItem.id !== item.id) {
            if (item.listPosition > destination.index) {
              const newPosition = descending ? item.listPosition + 1 : item.listPosition - 1;

              updateToDoLocal(getListItemVars(item, true, newPosition));
              updateToDoRemote(getListItemVars(item, false, newPosition));
            } else if (item.listPosition === destination.index) {
              const newPosition = descending ? item.listPosition - 1 : item.listPosition + 1;

              updateToDoLocal(getListItemVars(item, true, newPosition));
              updateToDoRemote(getListItemVars(item, false, newPosition));
            }
          } else {
            updateToDoLocal(getListItemVars(item, true, destination.index));
            updateToDoRemote(getListItemVars(item, false, destination.index));
          }
        }
      } else {
        // move item to another list

        for (let index in stateData[source.droppableId]) {
          const item = stateData[source.droppableId][index];

          // update item positions in source list
          if (item.listPosition > selectedItem.listPosition) {
            const newPosition = item.listPosition - 1;

            updateToDoLocal(getListItemVars(item, true, newPosition));
            updateToDoRemote(getListItemVars(item, true, newPosition));
          }
        }

        for (let index in stateData[destination.droppableId]) {
          const item = stateData[destination.droppableId][index];

          // update item positions in destination list
          if (item.listPosition >= destination.index) {
            const newPosition = item.listPosition + 1;

            updateToDoLocal(getListItemVars(item, true, newPosition));
            updateToDoRemote(getListItemVars(item, true, newPosition));
          }
        }

        const updatedToDo = {
          variables: {
            id: selectedItem.id,
            status: destination.droppableId,
            listPosition: destination.index,
          },
        };

        updateToDoLocal(updatedToDo);
        updateToDoRemote(updatedToDo);
      }
    }
  };

  const [updateToDoLocal] = useMutation(UPDATE_TODO_LOCAL);
  const [updateToDoRemote] = useMutation(UPDATE_TODO_REMOTE);

  const [updateLists] = useMutation(UPDATE_LISTS, {
    refetchQueries: [
      {
        query: GET_CONFIG,
        variables: { user: selectedUser },
      },
    ],
  });

  if (loading) return <LoadingMessage />;
  if (error) return <ErrorMessage />;

  return (
    <div styleName="container">
      <DragDropContext onDragEnd={onDragEnd}>
        <div styleName="lists">
          {renderLists(data)}

          {data.config[0].lists && data.config[0].lists.length < 6 && (
            <EditFields
              type="list"
              maxLength={14}
              handleSave={(item) =>
                updateLists({ variables: { user: selectedUser, lists: addItem(item, data.config[0].lists) } })
              }
            />
          )}
        </div>
      </DragDropContext>
    </div>
  );
};

export default ListContainer;
