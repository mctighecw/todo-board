import React from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import './styles.less';

import TopBar from 'Components/TopBar/TopBar';
import Board from 'Components/Board/Board';
import Info from 'Components/Info/Info';

const Main = ({ location }) => (
  <div styleName="container">
    <TopBar pathname={location.pathname} />
    <div styleName="content">
      <Switch>
        <Route path="/board" component={Board} />
        <Route path="/info" component={Info} />
        <Route render={() => <Redirect to="/board" />} />
      </Switch>
    </div>
  </div>
);

export default withRouter(Main);
