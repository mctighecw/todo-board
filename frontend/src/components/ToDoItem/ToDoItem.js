import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { checkUser, formatDate } from 'Utils/functions';

import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { Mutation } from 'react-apollo';
import { DELETE_TODO_ITEM } from 'Graphql/mutations';
import { GET_CONFIG, GET_USER_TODOS } from 'Graphql/queries';

import CloseIcon from 'Assets/close-icon-black.svg';
import { LIGHT_BLUE, LIGHT_GREY } from '../../styles/constants';
import './styles.less';

const ToDoItem = ({ item, index }) => {
  const client = useApolloClient();
  const selectedUser = checkUser();
  const { loading, error, data } = useQuery(GET_CONFIG, { variables: { user: selectedUser } });

  const getItemStyle = (isDragging, draggableStyle) => ({
    background: isDragging ? LIGHT_BLUE : LIGHT_GREY,
    ...draggableStyle,
  });

  const getCategStyle = (category) => {
    const index = data.config[0].categories.indexOf(category);
    let result = 'cat_0';

    if (index > 0) {
      result = `cat_${index}`;
    }
    return result;
  };

  if (loading || error) return null;

  return (
    <Mutation
      mutation={DELETE_TODO_ITEM}
      variables={{ id: item.id }}
      refetchQueries={() => {
        return [
          {
            query: GET_USER_TODOS,
            variables: { user: selectedUser },
          },
        ];
      }}
    >
      {(deleteToDo) => (
        <Draggable draggableId={item.id} index={index}>
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
              styleName="list-item"
              style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
            >
              <div onClick={deleteToDo} styleName="delete">
                <img src={CloseIcon} alt="Delete" />
              </div>
              <div styleName="content">{item.content}</div>
              <div styleName="tags">
                <div styleName={`category ${getCategStyle(item.category)}`}>{item.category}</div>
                <div styleName={`priority ${item.priority}`}>{item.priority}</div>
                <div styleName="date">{formatDate(item.createdAt)}</div>
              </div>
            </div>
          )}
        </Draggable>
      )}
    </Mutation>
  );
};

export default ToDoItem;
