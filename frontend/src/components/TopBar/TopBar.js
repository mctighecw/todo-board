import React from 'react';
import { Link } from 'react-router-dom';
import ToDoIcon from 'Assets/todo-icon.svg';
import './styles.less';

const TopBar = ({ pathname }) => {
  const renderLinks = () => {
    const links = ['board', 'info'];
    const result = [];

    links.map((item, index) => {
      const link = `/${item}`;
      const style = pathname === link ? 'link active' : 'link';

      result.push(
        <Link key={index} to={link} styleName={style}>
          {item}
        </Link>
      );
    });
    return result;
  };

  return (
    <div styleName="container">
      <div styleName="content">
        <div styleName="left-box">
          <img src={ToDoIcon} />
          <div styleName="text">To Do Board</div>
        </div>

        <div styleName="right-box">{renderLinks()}</div>
      </div>
    </div>
  );
};

export default TopBar;
