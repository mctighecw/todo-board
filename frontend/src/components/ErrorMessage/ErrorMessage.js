import React from 'react';
import ErrorIcon from 'Assets/error-icon.svg';
import './styles.less';

const ErrorMessage = () => (
  <div styleName="container">
    <img src={ErrorIcon} alt="!" />
    <div styleName="message">Error loading data</div>
  </div>
);

export default ErrorMessage;
