import React from 'react';
import './styles.less';

const MobileMessage = () => (
  <div styleName="container">
    <div styleName="content">
      <div styleName="text margin-bottom">Unfortunately this app does not have a mobile version yet.</div>
      <div styleName="text">Please try again with a tablet, laptop, or desktop.</div>
    </div>
  </div>
);

export default MobileMessage;
