import React from 'react';

import ReactLogo from 'Assets/tech/react.svg';
import GraphQLLogo from 'Assets/tech/graphql.svg';
import PythonLogo from 'Assets/tech/python.svg';
import MongoDBLogo from 'Assets/tech/mongodb.svg';
import Footer from './Footer/Footer';

import './styles.less';

const Info = () => (
  <div styleName="container">
    <div styleName="content">
      <div styleName="top-heading">General Info</div>

      <div styleName="general-info">
        <div styleName="heading">Users</div>
        <div styleName="text">
          The drop down menu on the top shows all users and lets you change the selected user. You can click
          on the blue edit icon to add a new one. The new name must be different from existing ones.
        </div>
      </div>

      <div styleName="general-info">
        <div styleName="heading">Categories</div>
        <div styleName="text">
          Each to do item is assigned a category. From the filters menu, the user can select a category and
          filter the relevant to do items.
        </div>
        <div styleName="text">
          When a user is created, the the default categories are <span>All</span>, <span>Personal</span>{' '}
          and <span>Family</span>. A new one can be added by clicking on the blue edit icon to the right. There
          can be up to eight categories total. Existing categories can be deleted by clicking on the blue
          icon, followed by the <span>Remove</span> button, and finally on the black x above the category to
          be removed. (Note, <span>All</span> cannot be deleted.)
        </div>
      </div>

      <div styleName="general-info">
        <div styleName="heading">Lists</div>
        <div styleName="text">
          To do items are sorted by lists, based on the to do item status. By default, the lists
          are <span>Backlog</span>, <span>In Progress</span> and <span>Finished</span>. Other lists
          (e.g. <span>Postponed</span>) can also be added. In total, there can be up to six lists at a time. To
          delete a list, the <span>All</span> filter must be selected and the list to be deleted must be
          empty. Then click on the white x icon in the upper right part of the list.
          (Note, <span>Backlog</span> cannot be deleted.)
        </div>
      </div>

      <div styleName="general-info">
        <div styleName="heading">To Do Items</div>
        <div styleName="text">
          To add a new to do item, click on the big red button. All fields are required: a short description
          of the to do item, the category, its priority level, as well as the starting list (
          <span>Backlog</span> by default). After clicking on the <span>Save</span> button, the item will
          appear. Each to do item can be reordered within the same list or moved to a different one, as many
          times as the user wishes. It can be deleted by clicking on the black x in the upper right corner.
        </div>
      </div>

      <div styleName="top-heading extra-margin">App Info</div>

      <div styleName="tech-overview">
        This app was built with modern frontend, backend, and database technologies.
      </div>

      <div styleName="technologies">
        <div styleName="box">
          <img src={ReactLogo} alt="React" />
          <div styleName="text">
            React frontend
            <br />
            JavaScript library
          </div>
        </div>

        <div styleName="box">
          <img src={GraphQLLogo} alt="GraphQL" />
          <div styleName="text">
            GraphQL for
            <br />
            database interactions
          </div>
        </div>

        <div styleName="box">
          <img src={PythonLogo} alt="Python" />
          <div styleName="text">
            Python Flask
            <br />
            backend framework
          </div>
        </div>

        <div styleName="box">
          <img src={MongoDBLogo} alt="MongoDB" />
          <div styleName="text">
            MongoDB
            <br />
            document database
          </div>
        </div>
      </div>

      <Footer />
    </div>
  </div>
);

export default Info;
