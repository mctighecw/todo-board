import React from 'react';
import './styles.less';

const Footer = () => (
  <div styleName="footer">
    <div styleName="copyright">&copy; 2019 Christian McTighe. Coded by Hand.</div>
  </div>
);

export default Footer;
