import React, { useState } from 'react';
import { useApolloClient, useQuery } from '@apollo/react-hooks';
import { GET_CONFIG_AND_USERS } from 'Graphql/queries';
import { checkUser } from 'Utils/functions';

import SmallButton from 'Components/shared/buttons/Small/Small';
import OutlineButton from 'Components/shared/buttons/Outline/Outline';
import Modal from 'Components/Modal/Modal';

import EditIcon from 'Assets/edit-icon.svg';
import './styles.less';

const EditFields = ({ type, maxLength, handleSave }) => {
  const client = useApolloClient();
  const selectedUser = checkUser();
  const { loading, error, data } = useQuery(GET_CONFIG_AND_USERS, { variables: { user: selectedUser } });

  const [editing, setEditing] = useState(false);
  const [label, setLabel] = useState('');
  const [showModal, setShowModal] = useState(false);

  const handleCheckIfExists = () => {
    const term = label.toLowerCase();
    let result = false;

    if (type === 'category') {
      result = data.config[0].categories.includes(term);
    } else if (type === 'list') {
      result = data.config[0].lists.includes(term);
    } else if (type === 'user') {
      const users = [];

      for (let index in data.users) {
        users.push(data.users[index].username);
      }
      result = users.includes(term);
    }
    return result;
  };

  const handleSaveValue = () => {
    const exists = handleCheckIfExists();

    if (exists) {
      setShowModal(true);
    } else {
      const term = label.toLowerCase();
      handleSave(term);
      handleClickClose();
    }
  };

  const handleClickClose = () => {
    if (data.remove_category) {
      client.writeData({ data: { remove_category: !data.remove_category } });
    }

    setEditing(false);
    setLabel('');
  };

  const handleClickRemoveCategory = () => {
    if (data.remove_category) {
      setEditing(false);
      setLabel('');
    }
    client.writeData({ data: { remove_category: !data.remove_category } });
  };

  if (loading || error) return null;

  return (
    <div styleName="container">
      {!editing ? (
        <div styleName="edit-icon" onClick={() => setEditing(true)}>
          <img src={EditIcon} alt="Edit" />
        </div>
      ) : (
        <div styleName={`editing-box ${type}`}>
          <input
            type="text"
            value={label}
            placeholder={`+ ${type}`}
            autoFocus={true}
            autoComplete="off"
            maxLength={maxLength}
            disabled={type === 'category' && data.config[0].categories.length > 7}
            onChange={(event) => setLabel(event.target.value)}
          />
          <SmallButton
            label="Add"
            disabled={label === '' || (type === 'category' && data.config[0].categories.length > 8)}
            onClickMethod={handleSaveValue}
          />
          <SmallButton label="Cancel" onClickMethod={handleClickClose} />

          {type === 'category' && (
            <SmallButton
              label={data.remove_category ? 'Done' : 'Remove'}
              onClickMethod={handleClickRemoveCategory}
            />
          )}
        </div>
      )}

      {showModal && (
        <Modal text={`Sorry, that ${type} already exists!`} onClickClose={() => setShowModal(false)} />
      )}
    </div>
  );
};

export default EditFields;
