import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { unmakeKey, addItem, removeItem } from 'Utils/functions';

import UserDropdown from 'Components/shared/inputs/UserDropdown/UserDropdown';
import EditFields from 'Components/EditFields/EditFields';
import NewToDo from 'Components/NewToDo/NewToDo';

import CloseIcon from 'Assets/close-icon-black.svg';
import './styles.less';

import { ADD_USER, UPDATE_CATEGORIES } from 'Graphql/mutations';
import { GET_USERS, GET_CONFIG, GET_CONFIG_AND_USERS } from 'Graphql/queries';

const BoardMenu = ({ selectedUser, selectedFilter, handleSetUser, handleSetFilter }) => {
  const { loading, error, data } = useQuery(GET_CONFIG_AND_USERS, { variables: { user: selectedUser } });

  const [addUser] = useMutation(ADD_USER, {
    refetchQueries: [
      {
        query: GET_USERS,
      },
    ],
  });

  const [updateCategories] = useMutation(UPDATE_CATEGORIES, {
    refetchQueries: [
      {
        query: GET_CONFIG,
        variables: { user: selectedUser },
      },
    ],
  });

  const handleChangeUser = (event) => {
    const { value } = event.target;
    handleSetUser(value);
  };

  if (loading || error) return null;

  return (
    <div styleName="container">
      <div styleName="user-dropdown">
        <div styleName="users-heading">User</div>

        <UserDropdown value={selectedUser} options={data.users} onChangeMethod={handleChangeUser} />

        <EditFields
          type="user"
          maxLength={10}
          handleSave={(item) => addUser({ variables: { username: item } })}
        />
      </div>
      <div styleName="filters-heading">Filter by category</div>
      <div styleName="filters-row">
        {data.config[0].categories.map((item, index) => {
          const style = selectedFilter === item ? `link active` : `link normal`;

          return (
            <div key={index} styleName={style}>
              {data.remove_category && item !== 'all' && (
                <img
                  src={CloseIcon}
                  alt="x"
                  styleName="delete"
                  onClick={(event) =>
                    updateCategories({
                      variables: {
                        user: selectedUser,
                        categories: removeItem(item, data.config[0].categories),
                      },
                    })
                  }
                />
              )}
              <div onClick={() => handleSetFilter(item)}>{item}</div>
            </div>
          );
        })}

        <EditFields
          type="category"
          maxLength={10}
          handleSave={(item) =>
            updateCategories({
              variables: { user: selectedUser, categories: addItem(item, data.config[0].categories) },
            })
          }
        />
      </div>
      <NewToDo />
    </div>
  );
};

export default BoardMenu;
