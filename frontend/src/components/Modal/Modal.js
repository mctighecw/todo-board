import React from 'react';
import ModalButton from 'Components/shared/buttons/Modal/Modal';
import CloseIcon from 'Assets/close-icon-black.svg';
import styles from './styles.less';

const Modal = ({ text, onClickClose }) => (
  <div styleName="modal-background">
    <div styleName="box">
      <img src={CloseIcon} alt="" styleName="close" onClick={onClickClose} />

      <div styleName="content">
        <div styleName="text">{text}</div>

        <ModalButton label="Close" onClickMethod={onClickClose} />
      </div>
    </div>
  </div>
);

export default Modal;
