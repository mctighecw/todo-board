import React, { useState } from 'react';
import BigButton from 'Components/shared/buttons/Big/Big';
import NewBox from './NewBox/NewBox';
import './styles.less';

const NewToDo = () => {
  const [showBox, setShowBox] = useState(false);

  return (
    <div styleName="container">
      <BigButton label={showBox ? 'Adding...' : 'New to do'} onClickMethod={() => setShowBox(true)} />
      {showBox && <NewBox onClickClose={() => setShowBox(false)} />}
    </div>
  );
};

export default NewToDo;
