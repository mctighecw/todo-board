import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { checkUser, makeKey } from 'Utils/functions';

import { Mutation } from 'react-apollo';
import { ADD_TODO_ITEM } from 'Graphql/mutations';
import { GET_CONFIG, GET_USER_TODOS } from 'Graphql/queries';

import Dropdown from 'Components/shared/inputs/Dropdown/Dropdown';
import MediumButton from 'Components/shared/buttons/Medium/Medium';
import Modal from 'Components/Modal/Modal';
import './styles.less';

const NewBox = ({ onClickClose }) => {
  const selectedUser = checkUser();
  const { loading, error, data } = useQuery(GET_CONFIG, { variables: { user: selectedUser } });

  const [content, setContent] = useState('');
  const [category, setCategory] = useState('');
  const [priority, setPriority] = useState('');
  const [status, setStatus] = useState('backlog');
  const [showModal, setShowModal] = useState(false);

  const handleSave = (addToDo) => {
    if (content === '' || category === '' || priority === '' || status === '') {
      setShowModal(true);
    } else {
      addToDo();
      onClickClose();
    }
  };

  if (loading || error) return null;

  return (
    <div styleName="new-box">
      <div styleName="content">
        <div styleName="heading">New item</div>

        <input
          type="text"
          value={content}
          name="content"
          placeholder="Description"
          autoFocus={true}
          autoComplete="off"
          maxLength={20}
          onChange={(event) => setContent(event.target.value)}
        />

        <Dropdown
          placeholder="Category"
          value={category}
          name="category"
          options={data.config[0].categories.filter((item) => item !== 'all')}
          onChangeMethod={(event) => setCategory(event.target.value)}
        />

        <Dropdown
          placeholder="Priority"
          value={priority}
          name="priority"
          options={['low', 'medium', 'high']}
          onChangeMethod={(event) => setPriority(event.target.value)}
        />

        <Dropdown
          placeholder="Starting list"
          value={status}
          name="status"
          options={data.config[0].lists}
          onChangeMethod={(event) => setStatus(event.target.value)}
        />

        <div styleName="buttons">
          <Mutation
            mutation={ADD_TODO_ITEM}
            variables={{
              content,
              category,
              priority,
              status: makeKey(status),
              user: selectedUser,
            }}
            refetchQueries={() => {
              return [
                {
                  query: GET_USER_TODOS,
                  variables: { user: selectedUser },
                },
              ];
            }}
          >
            {(addToDo) => <MediumButton label="Save" type="full" onClickMethod={() => handleSave(addToDo)} />}
          </Mutation>

          <MediumButton label="Cancel" type="outline" onClickMethod={onClickClose} />
        </div>
      </div>

      {showModal && <Modal text="Please fill out all fields!" onClickClose={() => setShowModal(false)} />}
    </div>
  );
};

export default NewBox;
