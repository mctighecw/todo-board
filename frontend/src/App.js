import React from 'react';
import { ApolloProvider } from 'react-apollo';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import client from './graphql/client';

import Main from './components/Main/Main';
import './styles/global.css';

const App = () => (
  <ApolloProvider client={client}>
    <Router>
      <Route component={Main} />
    </Router>
  </ApolloProvider>
);

export default App;
