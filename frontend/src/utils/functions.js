import moment from 'moment';

const getOffset = (datestring) => {
  const offset = moment().utcOffset();
  const currentDateTime = moment
    .utc(datestring)
    .utcOffset(offset)
    .format();
  return currentDateTime;
};

export const formatDate = (datestring) => {
  const currentDateTime = getOffset(datestring);
  return moment(currentDateTime).format('DD-MMM-YYYY');
};

export const makeKey = (name) => {
  const key = name.replace(/ /g, '_');
  return key;
};

export const unmakeKey = (key) => {
  const name = key.replace(/_/g, ' ');
  return name;
};

export const addItem = (name, state) => {
  return [...state, name];
};

export const removeItem = (key, state) => {
  const name = unmakeKey(key);
  const newState = state.filter((item) => item !== name);
  return newState;
};

export const getListItemVars = (item, withStatus, listPosition) => {
  const { id } = item;
  const status = withStatus ? item.status : '';
  return { variables: { id, status, listPosition } };
};

export const checkUser = () => {
  const selectedUser = localStorage.getItem('selected_user');
  let defaultUser = selectedUser;

  if (!selectedUser) {
    defaultUser = 'jsmith';
    saveUser(defaultUser);
  }
  return defaultUser;
};

export const saveUser = (username) => {
  localStorage.setItem('selected_user', username);
};
