import { makeKey } from 'Utils/functions';

const filterCategories = (sortedData, selectedFilter, lists) => {
  const category = selectedFilter.toLowerCase();
  const keys = lists.map((item) => makeKey(item));
  let filteredData = {};

  keys.forEach((key) => {
    if (sortedData[key]) {
      filteredData[key] = sortedData[key].filter((item) => item.category === category);
    }
  });

  return filteredData;
};

export const filterData = (sortedData, selectedFilter, lists) => {
  let filteredData = {};

  if (selectedFilter === 'all') {
    filteredData = sortedData;
  } else {
    filteredData = filterCategories(sortedData, selectedFilter, lists);
  }

  return filteredData;
};

export const loadData = (data, lists) => {
  const keys = lists.map((item) => makeKey(item));
  const sortedData = {};
  const stateData = {};

  keys.forEach((key) => (sortedData[key] = []));

  // divide todos into different lists and sort them by listPosition
  for (let i = 0; i < data.length; i++) {
    sortedData[data[i].status].push({ ...data[i] });
    sortedData[data[i].status].sort((a, b) => a.listPosition - b.listPosition);
  }

  // reassign listPosition based on index in array, to ensure
  // that each item in list has the correct index
  for (let i = 0; i < Object.values(sortedData).length; i++) {
    const key = Object.keys(sortedData)[i];
    const values = Object.values(sortedData)[i];
    const reIndexed = [];
    values.forEach((item, index) => reIndexed.push({ ...item, listPosition: index }));
    stateData[key] = reIndexed;
  }

  return stateData;
};
