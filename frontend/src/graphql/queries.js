import gql from 'graphql-tag';

export const LOCAL_QUERY = gql`
  query {
    remove_category @client
  }
`;

export const GET_CONFIG = gql`
  query getConfig($user: String!) {
    config(user: $user) {
      categories
      lists
    }
    remove_category @client
  }
`;

export const GET_USERS = gql`
  query {
    users {
      username
    }
  }
`;

export const GET_CONFIG_AND_USERS = gql`
  query getConfigAndUsers($user: String!) {
    config(user: $user) {
      categories
      lists
    }
    users {
      username
    }
    remove_category @client
  }
`;

export const GET_CONFIG_AND_TODOS = gql`
  query getConfigAndTodos($user: String!) {
    config(user: $user) {
      categories
      lists
    }
    userTodos(user: $user) {
      id
      content
      category
      priority
      status
      listPosition
      createdAt
    }
    remove_category @client
  }
`;

export const GET_USER_TODOS = gql`
  query getUserTodos($user: String!) {
    userTodos(user: $user) {
      id
      content
      category
      priority
      status
      listPosition
      createdAt
    }
  }
`;
