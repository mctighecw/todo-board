import gql from 'graphql-tag';

export const ADD_TODO_ITEM = gql`
  mutation addToDo(
    $content: String!
    $category: String!
    $priority: String!
    $status: String!
    $user: String!
  ) {
    addToDo(content: $content, category: $category, priority: $priority, status: $status, user: $user) {
      todo {
        id
      }
    }
  }
`;

export const UPDATE_TODO_LOCAL = gql`
  mutation updateToDo($id: String!, $status: String, $listPosition: Int!) {
    updateToDo(id: $id, status: $status, listPosition: $listPosition) @client
  }
`;

export const UPDATE_TODO_REMOTE = gql`
  mutation updateToDo($id: String!, $status: String, $listPosition: Int!) {
    updateToDo(id: $id, status: $status, listPosition: $listPosition) {
      todo {
        id
      }
    }
  }
`;

export const DELETE_TODO_ITEM = gql`
  mutation deleteToDo($id: String!) {
    deleteToDo(id: $id) {
      todo {
        id
      }
    }
  }
`;

export const ADD_USER = gql`
  mutation addUser($username: String!) {
    addUser(username: $username) {
      user {
        username
      }
    }
  }
`;

export const UPDATE_CATEGORIES = gql`
  mutation updateCategories($user: String!, $categories: [String]!) {
    updateConfig(user: $user, categories: $categories, lists: []) {
      config {
        categories
      }
    }
  }
`;

export const UPDATE_LISTS = gql`
  mutation updateLists($user: String!, $lists: [String]!) {
    updateConfig(user: $user, categories: [], lists: $lists) {
      config {
        lists
      }
    }
  }
`;
