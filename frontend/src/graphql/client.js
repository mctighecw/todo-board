import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

import typeDefs from './local/typeDefs';
import resolvers from './local/resolvers';
import initialState from './local/initialState';

const prefix = process.env.NODE_ENV === 'development' ? 'http://localhost:9000' : '';
const uri = `${prefix}/graphql`;
const link = new HttpLink({ uri });
const cache = new InMemoryCache();

cache.writeData({ data: initialState });

const client = new ApolloClient({
  link,
  cache,
  typeDefs,
  resolvers,
});

export default client;
