const initialState = {
  allTodos: {
    __typename: 'ToDoType',
  },
  config: {
    __typename: 'ConfigType',
  },
  users: {
    __typename: 'UserType',
  },
  remove_category: false,
};

export default initialState;
