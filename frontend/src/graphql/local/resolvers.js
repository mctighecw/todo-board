import gql from 'graphql-tag';

const resolvers = {
  Mutation: {
    // Update todo item locally in cache
    updateToDo: (_root, variables, { cache, getCacheKey }) => {
      const id = getCacheKey({ __typename: 'ToDoType', id: variables.id });
      const fragment = gql`
        fragment updateToDoItem on ToDoType {
          id
          status
          listPosition
        }
      `;
      const todo = cache.readFragment({ fragment, id });
      const data = { ...todo, status: variables.status, listPosition: variables.listPosition };
      cache.writeData({ id, data });
      return null;
    },
  },
};

export default resolvers;
