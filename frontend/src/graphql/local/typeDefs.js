import gql from 'graphql-tag';

const typeDefs = `
  type UserType {
    username: String!
  }
  type ConfigType {
    categories: [String]!
    lists: [String]!
    user: String!
  }
  type ToDoType {
    id: String!
    content: String!
    category: String!
    priority: String!
    status: String!
    listPosition: Int!
    user: String!
    createdAt: String!
  }
  type Query {
    users: [UserType]
    config: [ConfigType]
    allTodos: [ToDoType]
    userTodos: [ToDoType]
    todoId: [ToDoType]
  }
  type Mutation {
    addToDo(todo: [ToDoType])
    updateToDo(todo: [ToDoType])
    deleteToDo(todo: [ToDoType])
    addUser(user: [UserType])
    deleteUser(user: [UserType])
    updateConfig(config: [ConfigType])
  }
`;

export default typeDefs;
