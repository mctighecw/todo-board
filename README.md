# README

A "to do" board with drag & drop functionality.

The user can create categories for the items (e.g. personal, work) as well as different lists (e.g. backlog, in progress, finished). _To do_ items can be moved within a list as well as from one list to another by drag & drop. Items and their position are saved to a database. In addition, multiple users can be created, each with separate categories, lists and _to do_ items.

## App Information

App Name: todo-board

Created: August 2019

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/todo-board)

Production: [Link](https://todo-board.mctighecw.site)

## Tech Stack

Frontend:

- React (all stateless functional components; use of React hooks)
- Less
- CSS Modules
- React Beautiful Drag 'n Drop
- Apollo GraphQL Client
- Prettier

Backend:

- Flask
- Graphene
- MongoDB

## To Run

_Frontend_

- Setup:

  ```
  $ yarn install
  ```

- Start:

  ```
  $ yarn start
  ```

_Backend_

- Add `.env` file to root directory

- Setup:

  _Note_: Python 3.6 recommended

  ```
  $ cd backend
  $ virtualenv venv
  $ source venv/bin/activate
  (venv) $ pip install -r requirements.txt
  ```

- Initialize db:

  ```
  $ cd backend
  $ source venv/bin/activate
  (venv) $ FLASK_ENV='development' python src/db/init_db.py
  ```

- If necessary, drop db (later on):

  ```
  $ cd backend
  $ source venv/bin/activate
  (venv) $ python src/db/drop_dev_db.py
  ```

- Start:

  ```
  $ cd backend
  $ source run_dev.sh
  ```

## Credits

- To do icon courtesy of _prettycons_ at [Flaticon]("https://www.flaticon.com)
- Pen icon courtesy of _Freepik_ at [Flaticon](https://www.flaticon.com/authors/freepik)

Last updated: 2025-02-17
