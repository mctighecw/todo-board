import graphene
from graphene_mongo import MongoengineObjectType

from src.models import (User as UserModel,
                        Config as ConfigModel,
                        ToDo as ToDoModel)


# types
class UserType(MongoengineObjectType):
    class Meta:
        model = UserModel

class ConfigType(MongoengineObjectType):
    class Meta:
        model = ConfigModel

class ToDoType(MongoengineObjectType):
    class Meta:
        model = ToDoModel


# queries
class Query(graphene.ObjectType):
    """
    All GraphQL queries.
    """
    users = graphene.List(UserType)
    config = graphene.List(ConfigType, user=graphene.String())
    all_todos = graphene.List(ToDoType)
    user_todos = graphene.List(ToDoType, user=graphene.String())
    todo_id = graphene.Field(ToDoType, id=graphene.String())

    def resolve_users(self, info):
        query = UserModel.objects()
        return query

    def resolve_config(self, info, user):
        query = ConfigModel.objects.filter(user=user).all()
        return query

    # todo queries
    def resolve_all_todos(self, info):
        query = ToDoModel.objects()
        return query

    def resolve_user_todos(self, info, user):
        query = ToDoModel.objects.filter(user=user).all()
        return query

    def resolve_todo_id(self, info, id):
        query = ToDoModel.objects.get(pk=id)
        return query


# mutations
class AddToDo(graphene.Mutation):
    """
    GraphQL mutation to add a new to do item.
    """
    class Arguments:
        content = graphene.String()
        category = graphene.String()
        priority = graphene.String()
        status = graphene.String()
        user = graphene.String()

    todo = graphene.Field(lambda: ToDoType)

    def mutate(self, info, content, category, priority, status, user):
        count = ToDoModel.objects.filter(user=user, status=status).count()

        new_todo = ToDoModel(content=content, category=category,
                    priority=priority, status=status, list_position=count,
                    user=user)

        new_todo.save()

        return AddToDo(todo=new_todo)


class UpdateToDo(graphene.Mutation):
    """
    GraphQL mutation to update todo item status and list position.
    """
    class Arguments:
        id = graphene.String()
        status = graphene.String()
        list_position = graphene.Int()

    todo = graphene.Field(lambda: ToDoType)

    def mutate(self, info, id, status, list_position):
        query = ToDoModel.objects.get(pk=id)

        if status is not "":
            query.status = status

        query.list_position = list_position
        query.save()

        return UpdateToDo(todo=query)


class DeleteToDo(graphene.Mutation):
    """
    GraphQL mutation to delete a to do item.
    """
    class Arguments:
        id = graphene.String()

    todo = graphene.Field(lambda: ToDoType)

    def mutate(self, info, id):
        query = ToDoModel.objects.get(pk=id)
        query.delete()

        return DeleteToDo(todo=query)


class AddUser(graphene.Mutation):
    """
    GraphQL mutation to add a new user along
    with a new config row.
    """
    class Arguments:
        username = graphene.String()

    user = graphene.Field(lambda: UserType)

    def mutate(self, info, username):
        new_user = UserModel(username=username)
        new_config = ConfigModel(user=username)

        new_user.save()
        new_config.save()

        return AddUser(user=new_user)

class DeleteUser(graphene.Mutation):
    """
    GraphQL mutation to delete a user along
    with the user's config and todo items.
    """
    class Arguments:
        username = graphene.String()

    user = graphene.Field(lambda: UserType)

    def mutate(self, info, username):
        user_query = UserModel.objects.get(username=username)
        config_query = ConfigModel.objects.get(user=username)
        todos_query = ToDoModel.objects.filter(user=username).all()

        user_query.delete()
        config_query.delete()
        todos_query.delete()

        return DeleteUser(user=user_query)


class UpdateConfig(graphene.Mutation):
    """
    GraphQL mutation to update config (categories and lists)
    for a specific user. If list is empty, then no update.
    """
    class Arguments:
        user = graphene.String()
        categories = graphene.List(graphene.String)
        lists = graphene.List(graphene.String)

    config = graphene.Field(lambda: ConfigType)

    def mutate(self, info, user, categories, lists):
        config = ConfigModel.objects.get(user=user)

        if len(categories) > 0:
            config.categories = categories
            config.save()

        if len(lists) > 0:
            config.lists = lists
            config.save()

        return UpdateConfig(config=config)


class Mutation(graphene.ObjectType):
    """
    All GraphQL mutations.
    """
    addToDo = AddToDo.Field()
    updateToDo = UpdateToDo.Field()
    deleteToDo = DeleteToDo.Field()
    addUser = AddUser.Field()
    deleteUser = DeleteUser.Field()
    updateConfig = UpdateConfig.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
