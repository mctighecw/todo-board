# Example GraphQL queries & mutations

# queries
query {
  users {
    id
    username
  }
}

query {
  config (user: "jsmith") {
    categories
    lists
  }
}

query {
  allTodos {
    id
    content
    priority
    category
    status
    user
    createdAt
  }
}

query {
  userTodos (user: "jsmith") {
    id
    content
    priority
    category
    status
    createdAt
  }
}

query {
  todoId (id: "5d56b9383d87b1650635f0b9") {
    id
    content
    priority
    status
    category
  }
}


# mutations
mutation addToDo {
    addToDo (
      content: "Washing",
      category: "personal",
      priority: "medium",
      status: "backlog",
      user: "swilliams"
    ) {
    todo {
      id
      content
    }
  }
}

mutation updateToDo {
    updateToDo (
      id: "5d619218ad8f51c4c017fa22",
      status: "finished",
      listPosition: 0
    ) {
    todo {
      id
      status
    }
  }
}

mutation deleteToDo {
    deleteToDo (
      id: "5d56b9383d87b1650635f0b9"
    ) {
    todo {
      id
    }
  }
}

mutation addUser {
    addUser (
      username: "bmcmann"
    ) {
    user {
      username
    }
  }
}

mutation deleteUser {
    deleteUser (
      username: "bmcmann",
    ) {
    user {
      id
      username
    }
  }
}

mutation updateConfig {
    updateConfig (
      user: "swilliams",
      categories: ["all", "personal", "family", "work"],
      lists: ["backlog", "in progress", "finished"]
    ) {
    config {
      categories
      lists
    }
  }
}
