# MongoDB terminal commands

# connect to db as admin
mongo -u admin --authenticationDatabase admin -p

# switch to db
use todos

# show users
db.getUsers()

# show collections (tables)
db.getCollectionNames()

# remove a collection (data)
db.data.drop()

# create a collection
db.createCollection("data")

# enter some data
db.data.insert({ content: "Laundry", category: "personal", priority: "high", status: "backlog" })
db.data.insert({ content: "Cleaning", category: "personal", priority: "low", status: "backlog" })
db.data.insert({ content: "Plan vacation", category: "family", priority: "medium", status: "backlog" })

# show everything in collection
db.data.find()

# add admin user
use admin
db.createUser(
    {
      user: "superadmin",
      pwd: passwordPrompt(),
      roles: [
        "userAdminAnyDatabase",
        "readWriteAnyDatabase",
        "backup",
        "restore"
      ]
    }
  )

# show users
use admin
db.system.users.find()

# change user password
use admin
db.changeUserPassword("superadmin", passwordPrompt())

# delete/remove user (don't delete same user as logged in)
db.dropUser("superadmin")
