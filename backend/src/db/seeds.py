users_data = [ 'jsmith', 'swilliams' ]

todos_data = [{
    'content': 'Laundry',
    'category': 'personal',
    'priority': 'high',
    'status': 'backlog',
    'list_position': 0,
    'user': 'jsmith'
},
{
    'content': 'Cleaning',
    'category': 'personal',
    'priority': 'low',
    'status': 'backlog',
    'list_position': 1,
    'user': 'jsmith'
},
{
    'content': 'Shopping',
    'category': 'personal',
    'priority': 'medium',
    'status': 'in_progress',
    'list_position': 0,
    'user': 'jsmith'
},
{
    'content': 'Plan vacation',
    'category': 'family',
    'priority': 'high',
    'status': 'in_progress',
    'list_position': 1,
    'user': 'jsmith'
},
{
    'content': 'House repairs',
    'category': 'family',
    'priority': 'medium',
    'status': 'backlog',
    'list_position': 2,
    'user': 'jsmith'
}]
