import os
import sys

root_dir = os.path.abspath(os.curdir)
sys.path.append(root_dir)

from src.models import User, ToDo, Config
from seeds import users_data, todos_data

def seed_db():
    """
    Seeds database with initial data
    """
    for item in users_data:
        print(item)
        new_user = User(username=item)
        new_config = Config(user=item)
        new_user.save()
        new_config.save()

    for item in todos_data:
        print(item['content'])

        new_todo = ToDo(
            content=item['content'],
            category=item['category'],
            priority=item['priority'],
            status=item['status'],
            list_position=item['list_position'],
            user=item['user'])

        new_todo.save()
