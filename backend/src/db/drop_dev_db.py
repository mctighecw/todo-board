import os
import pymongo

client = pymongo.MongoClient(
    'localhost',
    port=27017,
    username='admin',
    password='password',
    authSource='admin')

print('Dropping todos database collections...')

db = client.get_database('todos')
users = db.users
config = db.config
data = db.data

users.drop()
config.drop()
data.drop()
