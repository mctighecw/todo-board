import os
from pathlib import Path
from dotenv import load_dotenv

FLASK_ENV = os.getenv('FLASK_ENV')

if FLASK_ENV == 'development':
    root_dir = Path(__file__).parent.parent.parent
    env_file = os.path.join(root_dir, '.env')
    load_dotenv(env_file)

DB_HOST_PROD = os.getenv('DB_HOST_PROD')
DB_HOST_DEV = os.getenv('DB_HOST_DEV')
DB_AUTH = os.getenv('DB_AUTH')
DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')
