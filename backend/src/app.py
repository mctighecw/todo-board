import logging
import os

from flask import Flask
from flask_cors import CORS
from flask_graphql import GraphQLView
from flask_mongoengine import MongoEngine

from src.graphql.schema import schema
from src import flask_config


def create_app():
    app = Flask(__name__)
    app.config.from_object(flask_config)

    # environment
    FLASK_ENV = os.getenv('FLASK_ENV')

    # allow any origin for development environment
    if FLASK_ENV == 'development':
        CORS(app)

    # configure logger
    logging.basicConfig(format='%(asctime)s %(levelname)s [%(module)s %(lineno)d] %(message)s',
                        level=app.config['LOG_LEVEL'])

    # database
    db = MongoEngine(app)

    # GraphQL
    if FLASK_ENV == 'development':
        allow_graphiql = True
    else:
        allow_graphiql = False

    app.add_url_rule('/graphql',
                    view_func=GraphQLView.as_view(
                        'graphql',
                        schema=schema,
                        graphiql=allow_graphiql)
                    )

    return app
