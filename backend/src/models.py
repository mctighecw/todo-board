from datetime import datetime, timezone

from mongoengine import Document
from mongoengine.fields import (StringField, IntField,
    BooleanField, ListField, DateTimeField, ReferenceField)


class User(Document):
    meta = { 'collection': 'users' }

    username = StringField()


class Config(Document):
    meta = { 'collection': 'config' }

    categories = ListField(StringField(), default=lambda: ['all', 'personal', 'family'])
    lists = ListField(StringField(), default=lambda: ['backlog', 'in progress', 'finished'])
    user = StringField()


class ToDo(Document):
    meta = { 'collection': 'data' }

    content = StringField()
    category = StringField()
    priority = StringField()
    status = StringField()
    list_position = IntField()
    user = StringField()
    created_at = DateTimeField(default=datetime.now(timezone.utc).astimezone())
