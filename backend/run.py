import os
from src.app import create_app

if __name__ == '__main__':
    app = create_app()

    port = 9000
    FLASK_ENV = os.getenv('FLASK_ENV')
    development_mode = FLASK_ENV == 'development'

    print(f'--- Starting web server on port {port}')

    app.run(port=port, host='0.0.0.0', debug=development_mode, use_reloader=False)
