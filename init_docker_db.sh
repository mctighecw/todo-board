#!/bin/sh

echo Initializing database in container...

# First log into mongodb and create admin user
# docker exec -it todo-board_mongodb_1 bash

docker exec -it todo-board_backend_1 python ./src/db/init_db.py
